# Created By: Min Xu <xukmin@gmail.com>
# Date: 11/24/2014

.data
user_prompt: 
  .asciiz "Enter an integer.\n"
buffer:
  .space 1025
invalid_output: 
  .asciiz "Invalid Entry.\n"	# declaration for string
cache_array:
  .space 200

.text
.globl main

# System call constants
# SYS_PRINT_INT       =   1
# SYS_PRINT_STRING    =   4
# SYS_READ_INT        =   5
# SYS_READ_STRING     =   8
# SYS_EXIT            =   10
# SYS_PRINT_CHAR      =   11
# SYS_READ_CHAR       =   12

main:
  la $t0, cache_array
  addi $t1, $t0, 200
  li $t2, -1
loop:
  sw $t2, 0($t0)
  addi $t0, $t0, 4
  bne $t0, $t1, loop

  ## Prompt the user to enter an integer
  la $a0, user_prompt
  li $v0, 4 
  syscall

  ## Read the integer, plus a terminator, into the buffer
  la $a0, buffer
  li $a1, 1024
  li $v0, 5 
  syscall

  move $a0, $v0			# set $a0 as the given interger number N
  la $a1, cache_array		# set $a1 as the address of cache_array
  jal FiboDyn			# call FiboDyn with N and &cache_array		

  li $t0, -1			# load immediate -1 to temporary register $t0
  bne $v0, $t0, Valid_output	# if $v0 != -1, go to Valid_output
  la $a0, invalid_output	# load the address of output string
  li $v0, 4			# if $v0 == -1, syscall: output a string
  syscall
  j Exit			# gp to Exit

Valid_output:			
  move $a0, $v0			# set $v0 as the argument of syscall
  li $v0, 1			# if $v0 != -1, syscall: output the result
  syscall
  
Exit:
  li $v0, 10			# system call: exit
  syscall

FiboDyn:
  addi $sp, $sp, -16		# adjust stack for 4 items
  sw $ra, 8($sp)		# save the return address
  sw $a1, 4($sp)		# save the base address of cache_array
  sw $a0, 0($sp)		# save the argument N

  bge $a0, $zero, BeginRecur	# if N >= 0, go to BeginRecur
  addi $v0, $zero, -1           # if N < 0, return -1
  addi $sp, $sp, 16             # pop 4 items off stack
  jr $ra                        # return to caller

BeginRecur:
  sll $t1, $a0, 2               # $t1 = N * 4
  add $a1, $a1, $t1		# get the address of cache_array[N]
  lw $t2, 0($a1)		# load the value of cache_array[N] t0 $t2

  li $t3, -1			# load immediate -1 to $t3
  beq $t2, $t3, L1		# if cache_array[N] == -1, go to L1 
  move $v0, $t2			# if cache_array[N] != -1, move the value to $v0
  addi $sp, $sp, 16		# pop 4 items off stack
  jr $ra 			# return to caller

L1:
  bne $a0, $zero, L2		# if N != 0, go to L2
  add $v0, $zero, $zero		# if N == 0, return 0
  lw $a1, 4($sp)		# load $a1 from stack for saving
  sll $t1, $a0, 2               # $t0 = N * 4
  add $a1, $a1, $t1		# get the address of cache_array[N]
  sw $v0, 0($a1)		# save the result into cache_array[N]
  addi $sp, $sp, 16		# pop 4 items off stack
  jr $ra			# return to caller

L2:
  li $t0, 1			# load immediate 1 to $t0
  bne $a0, $t0, L3		# if N != 1, go to L3
  addi $v0, $zero, 1        	# if N == 1, return 1
  lw $a1, 4($sp)		# load $a1 from stack for saving
  sll $t1, $a0, 2               # $t0 = N * 4
  add $a1, $a1, $t1		# get the address of cache_array[N]
  sw $v0, 0($a1)		# save the result into cache_array[N]
  addi $sp, $sp, 16		# pop 4 items off stack
  jr $ra			# return to caller
  
L3:
  addi $a0, $a0, -1		# N >= 2: argument gets (N - 1)
  lw $a1, 4($sp)		# restore argument: base address of cache_array
  jal FiboDyn			# call Fibonacci with (N - 1)
  move $s0, $v0			# $s0 get the value of $v0

  sw $s0, 12($sp)		# save $s0 for calculating later

  lw $a0, 0($sp)		# restore argument N
  lw $a1, 4($sp)		# restore argument: base address of cache_array
  addi $a0, $a0, -2		# N >= 2: argument gets (N - 2)
  jal FiboDyn			# call Fibonacci with (N - 2)

  lw $a0, 0($sp)		# restore argument N
  lw $a1, 4($sp)		# restore argument: base address of cache_array
  lw $ra, 8($sp)		# restore the return address
  lw $s0, 12($sp)		# restore the value of Fibonacci(N - 1)
  addi $sp, $sp, 16		# adjust stack pointer to pop 4 items
 
  add $v0, $v0, $s0		# return Fibonacci(N - 1) + Fibonacci(N - 2)
  sll $t1, $a0, 2		# $t1 = N * 4
  add $a1, $a1, $t1		# get the address of cache_array[N]
  sw $v0, 0($a1)		# save the result into cache_array[N]
  jr $ra			# return to the caller

