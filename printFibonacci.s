# Created By: Min Xu <xukmin@gmail.com>
# Date: 11/26/2014

.data
user_prompt: 
  .asciiz "Enter an integer.\n"
buffer:
  .space 1025
invalid_output: 
  .asciiz "Invalid Entry.\n"	# declaration for string

.text
.globl main

# System call constants
#SYS_PRINT_INT       =   1
#SYS_PRINT_STRING    =   4
#SYS_READ_INT        =   5
#SYS_READ_STRING     =   8
#SYS_EXIT            =   10
#SYS_PRINT_CHAR      =   11
#SYS_READ_CHAR       =   12

main:
  ## Prompt the user to enter an integer
  la $a0, user_prompt
  li $v0, 4
  syscall

  ## Read the integer, plus a terminator, into the buffer
  la $a0, buffer
  li $a1, 1024
  li $v0, 5
  syscall

  move $a0, $v0                 # set $s0 as the given interger number N
  jal Fibonacci                 # call Fibonacci with N                 

  li $t0, -1                    # load immediate -1 to temporary register $t0
  bne $v0, $t0, Valid_output    # if $v0 != -1, go to Valid_output
  la $a0, invalid_output        # load the address of output string
  li $v0, 4                     # if $v0 == -1, syscall: output a string
  syscall
  j Exit                        # gp to Exit

Valid_output:
  move $a0, $v0                 # set $v0 as the argument of syscall
  li $v0, 1                     # if $v0 != -1, syscall: output the result
  syscall

Exit:
  li $v0, 10                    # system call: exit
  syscall

Fibonacci:
  addi $sp, $sp, -12		# adjust stack for 3 items
  sw $ra, 4($sp)		# save the return address
  sw $a0, 0($sp)		# save the argument N

  bge $a0, $zero, L1		# if N >= 0, go to L1
  addi $v0, $zero, -1		# return -1
  addi $sp, $sp, 12		# pop 3 items off stack
  jr $ra

L1:
  bne $a0, $zero, L2		# if N != 0, go to L2
  add $v0, $zero, $zero		# return 0
  addi $sp, $sp, 12		# pop 3 items off stack
  jr $ra

L2:
  li $t0, 1
  bne $a0, $t0, L3		# if N != 1, go to L3
  addi $v0, $zero, 1        	# return 1
  addi $sp, $sp, 12		# pop 3 items off stack
  jr $ra
  
L3:
  addi $a0, $a0, -1		# N >= 2: argument gets (N - 1)
  jal Fibonacci			# call Fibonacci with (N - 1)
  move $s0, $v0			# $s0 get the value of $v0

  sw $s0, 8($sp)		# save $s0

  lw $a0, 0($sp)		# return from jal: restore argument N
  addi $a0, $a0, -2		# N >= 2: argument gets (N - 2)
  jal Fibonacci			# call Fibonacci with (N - 2)

  lw $ra, 4($sp)		# restore the return address
  lw $s0, 8($sp)		# restore the value of Fibonacci(N - 1)
  addi $sp, $sp, 12		# adjust stack pointer to pop 3 items
 
  add $v0, $v0, $s0		# return Fibonacci(N - 1) + Fibonacci(N - 2)
  jr $ra			# return to the caller
