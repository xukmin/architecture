# Created By: Min Xu <xukmin@gmail.com>
# Date: 11/29/2014

.data
user_prompt:
  .asciiz "Enter an integer.\n"
buffer:
  .space 1025
output_no:
  .asciiz "No, it is not a Fibonacci number.\n"
output_yes:
  .asciiz "Yes, it is a Fibonacci number.\n"

.text
.globl main

main:
  ## Prompt the user to enter an integer
  la $a0, user_prompt
  li $v0, 4
  syscall

  ## Read the integer, plus a terminator, into the buffer
  la $a0, buffer
  li $a1, 1024
  li $v0, 5
  syscall

  beq $v0, $zero, Valid_output
  move $a0, $v0                 # set $a0 as the given interger number N
  jal CheckFibo                 # call CheckFibo with N

  bne $v0, $zero, Valid_output    # if $v0 != -1, go to Valid_output
  la $a0, output_no             # load the address of output string
  li $v0, 4                     # if $v0 == -1, syscall: output a string
  syscall
  j Exit                        # gp to Exit

Valid_output:
  la $a0, output_yes             # load the address of output string
  li $v0, 4                     # if $v0 == -1, syscall: output a string
  syscall

Exit:
  li $v0, 10                    # system call: exit
  syscall

CheckFibo:
  ## Set the lower and upper bound of loop
  sll $s0, $a0, 1               # set $s0 as lower_plus = 2 * N
  addi $t0, $a0, 1              # $t0 = $a0 + 1
  addi $t1, $a0, -1             # $t1 = $a0 - 1
  sll $s1, $t0, 1
  add $s1, $s1, $a0
  addi $s1, $s1, 2              # set $s1 as upper_plus = 3 * (N + 1) + 1
  sll $s2, $t1, 1               # set $s2 as lower_minus = 2 * (N - 1)
  add $s3, $s0, $a0   
  addi $s3, $s3, 1              # set $s3 as upper_minus = 3 * N + 1

  mul $t2, $a0, $a0             # temporary $t2 = N * N
  li $t3, 5
  mul $t4, $t2, $t3
  addi $s4, $t4, 4              # set $s4 as 5 * N * N + 4
  addi $s5, $t4, -4             # set $s5 as 5 * N * N - 4

loop1:
  bgt $s0, $s1, loop2           # if $s0 > upper_plus bound, go to loop2
  div $t5, $s4, $s0             # $t5 = the quotient of $s4/$s0 
  rem $t6, $s4, $s0             # $t6 = the remainder of $s4/$s0
  beq $t5, $s0, L1              # if $t5 == $s0, go to L1
  addi $s0, $s0, 1              # else $s0 = $s0 + 1
  j loop1                       # go to loop1

L1:
  beq $t6, $zero, Exit1         # if the remainder is zero, go to Exit1
  addi $s0, $s0, 1              # $s0 = $s0 + 1
  j loop1                       # go to loop1

loop2:
  bgt $s2, $s3, Exit2           # if $s2 > upper_minus bound, go to Exit2
  div $t5, $s5, $s2             # $t5 = the quotient of $s5/$s2
  rem $t6, $s5, $s2             # $t6 = the remainder of $s5/$s2
  beq $t5, $s2, L2              # if $t5 == $s2, go to L2
  addi $s2, $s2, 1              # else $s2 = $s2 + 1
  j loop2                       # go to loop2

L2:
  beq $t6, $zero, Exit1         # if the remainder is zero, go to Exit1
  addi $s2, $s2, 1              # $s2 = $s2 + 1
  j loop2                       # go to loop2

Exit2:
  move $v0, $zero               # set $v0 = 0
  jr $ra                        # return

Exit1:
  li $v0, 1                     # set $v0 = 1
  jr $ra                        # return
